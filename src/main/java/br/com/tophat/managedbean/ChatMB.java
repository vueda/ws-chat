package br.com.tophat.managedbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean(name="chatMB")
@ViewScoped
public class ChatMB {

	private Logger logger = LoggerFactory.getLogger(ChatMB.class);
	
	private String message;

	/**
	 * URL raiz do websocket
	 */
	private static final String WEBSOCKET_URL = "ws://ws-chat.vueda.cloudbees.net/chat/";
	//private static final String WEBSOCKET_URL = "ws://localhost:8080/ws-chat/chat/";
	
	public String getUrl(){
		return WEBSOCKET_URL;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
	
}
