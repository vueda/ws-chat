
package br.com.tophat;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ServerEndpoint("/chat/{roomName}/{username}")
public class ChatServer {
	
	private Logger logger = LoggerFactory.getLogger(ChatServer.class);

	private static Queue<Session> openSessions = new ConcurrentLinkedQueue<Session>();
	
	@OnOpen
	public void onOpen(@PathParam("username") String username, Session session, EndpointConfig conf){
		logger.info("User " + username + " connected."); 
		session.getUserProperties().put("username", username);
		openSessions.add(session);	
		broadcast(username + " conectou.");
	}
	
	@OnClose
	public void onClose(Session session, CloseReason reason){
		logger.info(session.getUserProperties().get("username") + " disconnected.");
		openSessions.remove(session);
		broadcast(session.getUserProperties().get("username") + " desconectou.");
	}
	
	@OnError
	public void onError(Session session, Throwable error){
		logger.error("Error connecting", error); 		
		openSessions.remove(session);	
	}

	@OnMessage
	public void onMessage(String message, Session session){
		broadcast((String)session.getUserProperties().get("username") + ": " +  message);
	}
	
	private void broadcast(String message){
		for(Session session : openSessions){
			try {
				session.getBasicRemote().sendText(message);
			} catch (IOException e) {
				logger.error("Error broadcasting message", e);
			}
		}
	}
}
